//
//  AppDelegate.h
//  图鸦
//
//  Created by brother on 15/10/16.
//  Copyright (c) 2015年 com.qizhi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) NSString *beforeName;

@end

