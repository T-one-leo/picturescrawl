//
//  AppDelegate.m
//  图鸦
//
//  Created by brother on 15/10/16.
//  Copyright (c) 2015年 com.qizhi. All rights reserved.
//

#import "AppDelegate.h"
#import "CCSecondViewController.h"
#import "UserMessageViewController.h"
@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [[UIApplication sharedApplication]setStatusBarHidden:YES withAnimation:UIStatusBarAnimationFade];
    // Override point for customization after application launch.
//    self.window = [[UIWindow alloc]initWithFrame:[[UIScreen mainScreen] bounds]];
//    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Login" bundle:nil];
//    NSLog(@"%@",NSHomeDirectory());
//    NSLog(@"%@",[[[NSBundle mainBundle] infoDictionary]objectForKey:@"CFBundleVersion"]);
//    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
//    double newVersion = [[[[NSBundle mainBundle]infoDictionary]objectForKey:@"CFBundleVersion"]doubleValue];
//    double oldVersion = [[userDefault valueForKey:@"CFBundleVersion"]doubleValue];
//    if (oldVersion == 0) {
//        [userDefault setValue:[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"] forKey:@"CFBundleVersion"];
//        [userDefault synchronize];
//        self.window.rootViewController = [storyBoard instantiateViewControllerWithIdentifier:@"newFeature"];
//    }else {
//        if (newVersion > oldVersion) {
//            [userDefault setValue:[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"] forKey:@"CFBundleVersion"];
//            [userDefault synchronize];
//            self.window.rootViewController = [storyBoard instantiateViewControllerWithIdentifier:@"newFeature"];
//        }else {
//            //            CCSecondViewController *secondView = [[CCSecondViewController alloc]init];
//            //            self.beforeName = secondView.username;
//            //            if (self.beforeName == nil) {
//            //                self.window.rootViewController = [storyBoard instantiateViewControllerWithIdentifier:@"mainViewController"];
//            //            } else {
//            self.window.rootViewController = [storyBoard instantiateViewControllerWithIdentifier:@"mainViewController"];
//            //            }
//        }
//    }
//    [self.window makeKeyAndVisible];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
