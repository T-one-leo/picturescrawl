//
//  TakePhoto.h
//  功能测试项目
//
//  Created by brother on 15/10/19.
//  Copyright © 2015年 com.zhaopenglei. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
typedef void (^sendPictureBlock)(UIImage *image);
@interface TakePhoto : NSObject<UIActionSheetDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate>
@property (nonatomic, copy)sendPictureBlock sendPicBlock;
+ (TakePhoto *)sharedModel;
+ (void)sharePicture:(sendPictureBlock)block;
@end
