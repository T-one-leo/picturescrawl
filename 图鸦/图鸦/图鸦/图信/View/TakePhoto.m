//
//  TakePhoto.m
//  功能测试项目
//
//  Created by brother on 15/10/19.
//  Copyright © 2015年 com.zhaopenglei. All rights reserved.
//

#import "TakePhoto.h"
#import "TalkViewController.h"
#define AppRootView ([[[[[UIApplication sharedApplication] delegate] window] rootViewController] view])
#define AppRootViewController ([[[[UIApplication sharedApplication] delegate] window] rootViewController])
@implementation TakePhoto
{
    NSUInteger sourceType;
}

+ (TakePhoto *)sharedModel {
    static TakePhoto *sharedModel = nil;
    static dispatch_once_t oneToken;
    dispatch_once(&oneToken,^{
        sharedModel = [[self alloc]init];
    });
    return sharedModel;
}

+ (void)sharePicture:(sendPictureBlock)block {
    TakePhoto *tp = [TakePhoto sharedModel];
    tp.sendPicBlock = block;
    UIActionSheet *sheet;
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        sheet = [[UIActionSheet alloc] initWithTitle:@"设置图像" delegate:tp cancelButtonTitle:nil destructiveButtonTitle:@"取消" otherButtonTitles:@"拍照",@"相册中获取", nil];
    } else {
        sheet = [[UIActionSheet alloc]initWithTitle:@"设置图像" delegate:tp cancelButtonTitle:nil destructiveButtonTitle:@"取消" otherButtonTitles:@"相册中获取", nil];
    }
    sheet.tag = 255;
//    [sheet showInView:AppRootView];
    TalkViewController *talkViewController = [[TalkViewController alloc]init];
    [sheet showInView:talkViewController.view];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (actionSheet.tag == 255) {
        sourceType = UIImagePickerControllerSourceTypeCamera;
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
            switch (buttonIndex) {
                case 0:
                    return;
                case 1:
                    sourceType = UIImagePickerControllerSourceTypeCamera;
                    break;
                case 2:
                    sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                    break;
            }
        }else {
            if (buttonIndex == 0) {
                return;
            }else {
                sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
            }
        }
        UIImagePickerController *imagePickerController = [[UIImagePickerController alloc]init];
        imagePickerController.delegate = self;
        imagePickerController.allowsEditing = YES;
        imagePickerController.sourceType = sourceType;
        //代理传到TalkViewController里弹框，
        TalkViewController *talkViewController = [[TalkViewController alloc]init];
        [talkViewController presentViewController:imagePickerController animated:YES completion:NULL];
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    TakePhoto *takePhoto = [TakePhoto sharedModel];
    [picker dismissViewControllerAnimated:YES completion:^{
    }];
    UIImage *image = [info objectForKey:UIImagePickerControllerEditedImage];
    [takePhoto sendPicBlock](image);
}

@end
