//
//  FriendsViewController.h
//  图鸦
//
//  Created by brother on 15/10/22.
//  Copyright © 2015年 com.qizhi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FriendsViewController : UIViewController<UICollectionViewDelegate, UISearchBarDelegate>
{
    NSArray *collectionImages;
}
@property (weak, nonatomic) IBOutlet UICollectionView *myCollectionVw;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar2;
@property (strong, nonatomic) NSMutableArray* filteredTableData;
@property (nonatomic, assign) bool isFiltered;
@end
