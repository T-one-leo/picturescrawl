//
//  TalkViewController.m
//  图鸦
//
//  Created by brother on 15/10/22.
//  Copyright © 2015年 com.qizhi. All rights reserved.
//

#import "TalkViewController.h"
#import "TakePhoto.h"
#import "UserMessageViewController.h"
#import "AppDelegate.h"
@interface TalkViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@property (weak, nonatomic) IBOutlet UILabel *timeLab;
@property (weak, nonatomic) IBOutlet UIButton *talkBtn;
@property (weak, nonatomic) IBOutlet UILabel *friendName;

@end

@implementation TalkViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.talkBtn.hidden = YES;
    // Do any additional setup after loading the view.
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)takePhotoClick:(UIButton *)sender {
    [TakePhoto sharePicture:^(UIImage *HeadImage) {
        [self.imageView setImage:HeadImage];
        self.talkBtn.hidden = NO;
    }];
}
- (IBAction)back:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)talkBtnClick:(UIButton *)sender {
    
}
- (IBAction)moreClick:(UIButton *)sender {
    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:nil message:nil delegate:self cancelButtonTitle: @"取消" otherButtonTitles:@"TA的主页",@"拉黑（?。?）",nil];
    [alertView show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSLog(@"button index = %ld is clicked.......",(long)buttonIndex);
    if (buttonIndex == 1) {
        UIStoryboard *board = [UIStoryboard storyboardWithName:@"UserInfo" bundle:nil];
        NSLog(@"%@",board);
        UserMessageViewController *viewController = [board instantiateViewControllerWithIdentifier:@"test"];
        [self  presentViewController:viewController animated:YES completion:nil];
    }else if (buttonIndex == 2) {
        //取消好友
        NSLog(@"好友取消");
    }else {
        
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
