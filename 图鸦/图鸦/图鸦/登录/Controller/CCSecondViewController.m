//
//  CCSecondViewController.m
//  02-用户登陆
//
//  Created by brother on 15/9/7.
//  Copyright (c) 2015年 CC. All rights reserved.
//

#import "CCSecondViewController.h"
#import "ViewController.h"
@interface CCSecondViewController ()
@property (weak, nonatomic) IBOutlet UILabel *titleLab;

@end

@implementation CCSecondViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.titleLab.text=[NSString stringWithFormat:@"%@,欢迎回来！",self.username];
    NSLog(@"%@",self.userpassword);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)back:(UIButton *)sender {
    if (self.username == nil) {
        UIStoryboard *myStoryboard=self.storyboard;
        ViewController *viewController = [myStoryboard instantiateViewControllerWithIdentifier:@"mainViewController"];
        [self presentViewController:viewController animated:YES completion:nil];
    }else {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}


- (IBAction)replaceBtnClick:(UIButton *)sender {
    
}

@end
