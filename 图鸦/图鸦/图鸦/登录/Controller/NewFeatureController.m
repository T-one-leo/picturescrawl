//
//  NewFeatureController.m
//  JxbLovelyLogin
//
//  Created by brother on 15/10/18.
//  Copyright © 2015年 Peter. All rights reserved.
//

#import "NewFeatureController.h"

@interface NewFeatureController ()
@property (weak, nonatomic) IBOutlet UILabel *showVersion;

@end

@implementation NewFeatureController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.showVersion.text = [[[NSBundle mainBundle] infoDictionary]objectForKey:@"CFBundleVersion"];
    // Do any additional setup after loading the view.
}
- (IBAction)goToMainTabController:(UIButton *)sender {
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
