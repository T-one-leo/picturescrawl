//
//  PicView.h
//  图鸦
//
//  Created by brother on 15/10/26.
//  Copyright © 2015年 com.qizhi. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Pic;

@class PicView;

typedef void (^PicClickBlock)(PicView *);

@interface PicView : UIView

@property (nonatomic, copy) PicClickBlock picClickBlock;

@property (nonatomic, strong) Pic *pic;

+ (instancetype)picView;

+ (instancetype)picViewWithPic:(Pic *)pic;

@end
