//
//  PicView.m
//  图鸦
//
//  Created by brother on 15/10/26.
//  Copyright © 2015年 com.qizhi. All rights reserved.
//

#import "PicView.h"
#import "Pic.h"
@interface PicView()
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIButton *checkBtn;
@end
@implementation PicView

+ (instancetype)picViewWithPic:(Pic *)pic {
    NSBundle *bundle = [NSBundle mainBundle];
    PicView *picView = [[bundle loadNibNamed:@"PicView" owner:nil options:nil] lastObject];
    picView.pic = pic;
    return picView;
}

+ (instancetype)picView {
    return [self picViewWithPic:nil];
}

- (void)setPic:(Pic *)pic {
    _pic = pic;
    NSString *appIcon = [NSString stringWithFormat:@"%@.jpg",pic.image];
    self.imageView.image = [UIImage imageNamed:appIcon];
}

- (IBAction)checkBtn:(UIButton *)sender {
    if (self.checkBtn.selected == NO) {
        [self.checkBtn setSelected:YES];
        NSLog(@"1");
    }else {
        [self.checkBtn setSelected:NO];
        NSLog(@"2");
    }
    self.picClickBlock(self);
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
