//
//  Pic.m
//  图鸦
//
//  Created by brother on 15/10/26.
//  Copyright © 2015年 com.qizhi. All rights reserved.
//

#import "Pic.h"

@implementation Pic

- (instancetype)initWithDict:(NSDictionary *)dict {
    if (self = [super init]) {
        self.image = dict[@"icon"];
    }
    return self;
}

+ (instancetype)picWithDict:(NSDictionary *)dict {
    return [[self alloc] initWithDict:dict];
}

@end
