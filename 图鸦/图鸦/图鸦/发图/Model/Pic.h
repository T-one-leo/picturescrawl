//
//  Pic.h
//  图鸦
//
//  Created by brother on 15/10/26.
//  Copyright © 2015年 com.qizhi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Pic : NSObject

@property (nonatomic, copy) NSString *image;

- (instancetype)initWithDict:(NSDictionary *)dict;

+ (instancetype)picWithDict:(NSDictionary *)dict;

@end
