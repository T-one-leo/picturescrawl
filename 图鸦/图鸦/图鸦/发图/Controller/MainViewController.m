//
//  MainViewController.m
//  Test515
//
//  Created by silicon on 14-5-15.
//  Copyright (c) 2014年 silicon. All rights reserved.
//

#import "MainViewController.h"
#import "ImageLoader.h"
#import "SelectViewController.h"
#define MY_WIDTH [[UIScreen mainScreen] bounds].size.width
#define MY_HEIGHT [[UIScreen mainScreen] bounds].size.height

@implementation MainViewController
//@synthesize activityIndicator = _activityIndicator;
@synthesize myScrollView = _myScrollView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self.view setFrame:CGRectMake(0, 0, MY_WIDTH, MY_HEIGHT)];
    UIButton *cancelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    cancelBtn.frame = CGRectMake(0, 0, MY_WIDTH, 50);
    self.myScrollView = [MyScrollView shareInstance];
//    _myScrollView.aDelegaet = self;
    [cancelBtn setTitle:@"<<<<<<<<<<<<<<编辑>>>>>>>>>>>>>>" forState:UIControlStateNormal];
    [cancelBtn setTintColor:[UIColor whiteColor]];
    [cancelBtn setBackgroundColor:[UIColor blackColor]];
    [cancelBtn addTarget:self action:@selector(backBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:cancelBtn];
    [self.view addSubview:_myScrollView];
//    //创建菊花
//    self.activityIndicator = [[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(MY_WIDTH/2, MY_HEIGHT/2, 30, 30)];
//    _activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
//    _activityIndicator.hidesWhenStopped = YES;
//    [self.view addSubview:_activityIndicator];
}

- (void)backBtnClick {
    [self dismissModalViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//- (void)startMyLoading{
//    [_activityIndicator startAnimating];
//}
//
//- (void)stopMyLoading{
//    [_activityIndicator stopAnimating];
//}

@end







