//
//  SelectViewController.m
//  图鸦
//
//  Created by brother on 15/10/23.
//  Copyright © 2015年 com.qizhi. All rights reserved.
//

#import "SelectViewController.h"
#import "PicView.h"
#import "Pic.h"
#import "MainViewController.h"
@interface SelectViewController ()
@property (weak, nonatomic) IBOutlet UIButton *continueBtn;
@property (nonatomic, strong) NSArray *pics;
@property (nonatomic, assign) PicClickBlock clickBlock;
@end

@implementation SelectViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (BOOL)prefersStatusBarHidden {
    return YES; 
}

- (IBAction)back:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)picSelBtn:(UIButton *)sender {
    MainViewController *mainViewCon = [[MainViewController alloc] init];
    [self presentModalViewController:mainViewCon animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
