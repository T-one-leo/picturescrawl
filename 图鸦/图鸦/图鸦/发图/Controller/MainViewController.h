//
//  MainViewController.h
//  Test515
//
//  Created by silicon on 14-5-15.
//  Copyright (c) 2014年 silicon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyScrollView.h"

@interface MainViewController : UIViewController

//@property (strong, nonatomic) UIActivityIndicatorView *activityIndicator;

@property (strong, nonatomic) MyScrollView *myScrollView;

@end
