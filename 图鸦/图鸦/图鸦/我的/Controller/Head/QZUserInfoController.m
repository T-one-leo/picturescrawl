//
//  QZUserInfoController.m
//  图鸦
//
//  Created by brother on 15/10/20.
//  Copyright (c) 2015年 com.qizhi. All rights reserved.
//

#import "QZUserInfoController.h"
#import "QZHeadInfoCell.h"
#import "ModeifyHeadViewController.h"
#import "QZUserInfoCell.h"

@interface QZUserInfoController ()<UIActionSheetDelegate,UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UIPickerView *sexPicker;
@property (weak, nonatomic) IBOutlet UIToolbar *sexToolBar;


@property (nonatomic, strong)NSArray *sexArray;

@property (nonatomic, strong)NSDictionary *addressDict;

@property (nonatomic, strong)NSDictionary *detailAddDict;

//@property (nonatomic,strong) NSArray *titleArray;// 左边的标题

@property (nonatomic, strong)UIImage *headImage;


@property (nonatomic, strong)NSArray *UserInfoImg;

@property (nonatomic, strong)NSArray *UserInfoLab;
@end

@implementation QZUserInfoController

- (NSDictionary *)addressArray {

    if (_addressDict == nil) {
        NSString *path = [[NSBundle mainBundle] pathForResource:@"city" ofType:@"plist"];
        _addressDict = [NSMutableDictionary dictionaryWithContentsOfFile:path];
        NSArray *sortArray = [[_addressDict allKeys] sortedArrayUsingSelector:@selector(compare:)];
        
        NSMutableArray *mutArray = [NSMutableArray array];
        for (int i = 0; i < sortArray.count; i++) {
            NSDictionary *dic = _addressDict[sortArray[i]];
            NSArray *keyArr = [dic allKeys];
            [mutArray addObject:keyArr];
            NSLog(@"%@",mutArray);
        }
        _addressDict = (NSDictionary *)mutArray;
    }
    return _addressDict;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    self.titleArray = [NSArray arrayWithObjects:@[@"石头人",@"男",@"江苏 苏州",@"我爱祁智"],nil];
    
    self.sexArray = [NSArray arrayWithObjects:@"男",@"女", nil];
    
    self.UserInfoImg = [NSArray arrayWithObjects:@"11",@"12",@"13",@"14", nil];
    
    self.UserInfoLab = [NSArray arrayWithObjects:@"石头人",@"男",@"江苏 苏州",@"我", nil];
    
    self.headImage = [UIImage imageNamed:@"6"];
    
    self.sexPicker.hidden = YES;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section ==0) {
        return 1;
    }else {
        return 4;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 22;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 11;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0 && indexPath.row == 0) {
        return 72;
    }else {
        return 48;
    }
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
//
    if (indexPath.section == 0 && indexPath.row ==0)
    {
        QZHeadInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QZHeadInfoCell" forIndexPath:indexPath];
        cell.headIamge.image = self.headImage;
        return cell;
    }else {
       static NSString *identifier = @"userInfoCell";
        QZUserInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (cell == nil) {
            cell = [[QZUserInfoCell  alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:identifier];
        }
        cell.accessoryType = UITableViewCellAccessoryNone;
        if (indexPath.section == 1) {
            cell.userInfoImage.image = [UIImage imageNamed:self.UserInfoImg[indexPath.row]];
            cell.userInfoLab.text = self.UserInfoLab[indexPath.row];
            NSLog(@"cell = %@",cell.userInfoLab.text);
        }
        return cell;
    }
//        else if(indexPath.section == 1)
//        {
//            0//            if (indexPath.section == 1) {
//                cell.userInfoImage.image = [UIImage imageNamed:self.UserInfoImg[indexPath.row]];
//                cell.userInfoLab.text = self.UserInfoLab[indexPath.row];
//                return cell;
//    }
//
//
//}
}
   

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            [self startChoosePhoto];
        }
    }else if (indexPath.section == 1){  //姓名
        if (indexPath.row == 0) {
            [self performSegueWithIdentifier:@"modefyNameSegue" sender:nil];
        }else if (indexPath.row == 1) { //性别
            
            
            
        }else if (indexPath.row == 1) { // 地址
            
        }else {  //个性签名
            
        }
    }
}

//开始创建actionSheet
- (void)startChoosePhoto {
    UIActionSheet *choiceSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"拍照",@"从相册中选取", nil];
    [choiceSheet showInView:self.view];
}

//actionSheet的代理方法，用来设置每个点击按钮的触发事件
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    //构建图像选择器
    UIImagePickerController *pickerController = [[UIImagePickerController alloc] init];
    [pickerController setDelegate:(id)self];
    
    if (buttonIndex == 0) {
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
            pickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
        }
             [pickerController.view setTag:actionSheet.tag];
             [self.view.window.rootViewController presentViewController:pickerController animated:YES completion:nil];
    }else if (buttonIndex == 1) {
        pickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self.view.window.rootViewController presentViewController:pickerController animated:YES completion:nil];
    }else {
        [actionSheet setHidden:YES];
    }
}


//图像选择器选取好后，将图片数据拿过来
- (void)imagePickerController:(UIImagePickerController *)pickerController didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
//    QZHeadInfoCell *headInfo = [[QZHeadInfoCell alloc] init];
    UIImage *image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
    NSData *imgData = UIImageJPEGRepresentation(image, 0.5);
    self.headImage= [UIImage imageWithData:imgData];
    [self.tableView reloadData];
    [pickerController dismissViewControllerAnimated:YES completion:nil];

}

#pragma mark -- 更改信息的页面
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"modifySegue"]) {
        ModeifyHeadViewController *modify = segue.destinationViewController;
        modify.selectedRow = 0;
    }
}

//- (void)updateSex:(NSNotification *)notification {
//    if ([notification.object isEqualToString:@"男"]) {
//        self.infoArray[1][0] = @"男";
//    }else {
//        self.infoArray[1][0] = @"女";
//    }
//    [self.tableView reloadData];
//}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
