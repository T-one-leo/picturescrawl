//
//  QZUserDefaultTableViewController.m
//  图鸦
//
//  Created by brother on 15/10/20.
//  Copyright (c) 2015年 com.qizhi. All rights reserved.
//

#import "QZUserDefaultTableViewController.h"
#import "QZHeadCell.h"
#import "QZSettingCell.h"
#import "NewFriendsViewController.h"
#import "MyNewsViewController.h"
#import "MyPictureViewController.h"

@interface QZUserDefaultTableViewController ()

@property (nonatomic, strong)NSArray *settingImages;

@property (nonatomic, strong)NSArray *settingNames;

@end

@implementation QZUserDefaultTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.settingImages = [NSArray arrayWithObjects:@"1",@"2",@"3", nil];
    self.settingNames = [NSArray arrayWithObjects:@"新的朋友",@"我的消息",@"我的图片", nil];
    
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

- (IBAction)back:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    switch (section) {
        case 0:
            return 1;
            break;
        case 1:
            return 3;
            break;
        case 2:
            return 1;
            break;
        default:
            return 0;
            break;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 0) {
        QZHeadCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QZHeadCell" forIndexPath:indexPath];
        cell.headImageVIew.layer.cornerRadius = 10;
        cell.headImageVIew.layer.masksToBounds = YES;
        return cell;
    }else {
        QZSettingCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QZSettingCell" forIndexPath:indexPath];
        if (indexPath.section == 1) {
            cell.settingImage.image = [UIImage imageNamed:self.settingImages[indexPath.row]];
            cell.settingNameLab.text = self.settingNames[indexPath.row];
            return cell;
        }else if (indexPath.section == 2){
            cell.settingImage.image = [UIImage imageNamed:@"4"];
            cell.settingNameLab.text = @"设置";
            return cell;
        }else {
            return nil;
        }
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 1) {
        if (indexPath.row == 0) {
            [self performSegueWithIdentifier:@"newsFriendsSegue" sender:nil];
        }else
            if ( indexPath.row == 1) {
                [self performSegueWithIdentifier:@"myNewsSegue" sender:nil];
        }else if (indexPath.row == 2) {
            [self performSegueWithIdentifier:@"myPictureSegue" sender:nil];
        }
    }else if (indexPath.section == 2){
        [self performSegueWithIdentifier:@"settingSegue" sender:nil];
    }
}



- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 22;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 14;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        return 90;
    }else {
        return 50;
    }
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
