//
//  QZSettingCell.h
//  图鸦
//
//  Created by brother on 15/10/20.
//  Copyright (c) 2015年 com.qizhi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QZSettingCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *settingImage;

@property (weak, nonatomic) IBOutlet UILabel *settingNameLab;
@end
