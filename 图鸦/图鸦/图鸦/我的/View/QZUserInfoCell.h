//
//  QZUserInfoCell.h
//  图鸦
//
//  Created by brother on 15/10/21.
//  Copyright (c) 2015年 com.qizhi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QZUserInfoCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *userInfoImage;
@property (weak, nonatomic) IBOutlet UILabel *userInfoLab;


@end
