//
//  QZHeadCell.h
//  图鸦
//
//  Created by brother on 15/10/20.
//  Copyright (c) 2015年 com.qizhi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QZHeadCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *headImageVIew;
@property (weak, nonatomic) IBOutlet UILabel *userNameLab;
@property (weak, nonatomic) IBOutlet UILabel *userinfoLab;

@end
