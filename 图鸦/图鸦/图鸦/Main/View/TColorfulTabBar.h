//
//  TColorfulTabBar.h
//  图鸦
//
//  Created by brother on 15/10/29.
//  Copyright © 2015年 com.qizhi. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol tagDelegate <NSObject>
- (void)dS:(NSInteger)index;
@end
@interface TColorfulTabBar : UITabBar {
    id <tagDelegate> getTDelegate;
}
@property (nonatomic, retain) id<tagDelegate> getTDelegate;
@end
