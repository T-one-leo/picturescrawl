//
//  TColorfulTabBar.m
//  图鸦
//
//  Created by brother on 15/10/29.
//  Copyright © 2015年 com.qizhi. All rights reserved.
//
#import "TColorfulTabBar.h"
#import "TColorfulTabBar+Configuration.h"
#import <TKit.h>

@interface TColorfulTabBar () <UITabBarDelegate>

@property (weak, nonatomic) UIView *colorfulView;
@property (strong, nonatomic) UIView *colorfulMaskView;

@property (assign, nonatomic) NSInteger fromeIndex;
@property (assign, nonatomic) NSInteger toIndex;

@end

@implementation TColorfulTabBar
@synthesize getTDelegate;

- (instancetype)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    
    if (self) {
        
        [self setupColorView];
        [self setupMaskLayer];
    }
    
    return self;
}

- (void)didMoveToSuperview {
    [super didMoveToSuperview];
    self.delegate = self;
}

- (void)setupMaskLayer {
    
    CGFloat itemWidth = self.width / self.itemCount;
    
    UIView *colorMaskView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, itemWidth, self.height)];
    colorMaskView.backgroundColor = [UIColor blackColor];
    self.colorfulMaskView = colorMaskView;
    self.colorfulView.layer.mask = self.colorfulMaskView.layer;
}

- (void)setupColorView {
    
    UIView *colorView = [[UIView alloc] initWithFrame:self.bounds];
    [self addSubview:colorView];
    self.colorfulView = colorView;
    
    NSArray *colors = [self itemColors];
    CGFloat itemWidth = self.width / [self itemCount];
    
    for (int i = 0; i < [self itemCount]; i ++) {
        
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(itemWidth * i, 0, itemWidth, self.height)];
        view.backgroundColor = colors[i];
        [self.colorfulView addSubview:view];
    }
}

- (void)animation {
    
    CGFloat itemWidth = self.width / [self itemCount];
    CGFloat extraWidth = itemWidth / 4;
    
    CGRect scaleFrame = CGRectMake(self.colorfulMaskView.x, 0, itemWidth + extraWidth, self.height);
    CGRect toFrame = CGRectMake(self.toIndex * itemWidth, 0, itemWidth, self.height);
    
    if (self.fromeIndex > self.toIndex) {
        
        scaleFrame = CGRectMake(self.colorfulMaskView.x - extraWidth, 0, itemWidth + extraWidth, self.height);
    }
    
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        NSLog(@"开始移动");
        NSLog(@"来自%ld",self.fromeIndex);
        self.colorfulMaskView.frame = scaleFrame;
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.1 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            NSLog(@"结束移动");
            NSLog(@"来自%ld",self.toIndex);
            [getTDelegate dS:self.toIndex];
            self.colorfulMaskView.frame = toFrame;
        } completion:NULL];
    }];
}

- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item {
    NSInteger index = [self.items indexOfObject:item];
    self.fromeIndex = self.toIndex;
    self.toIndex = index;
    [self animation];
}

@end
