//
//  TabBarViewController.m
//  图鸦
//
//  Created by brother on 15/10/29.
//  Copyright © 2015年 com.qizhi. All rights reserved.
//

#import "TabBarViewController.h"
#import "TColorfulTabBar.h"
#import <TKit.h>
@interface TabBarViewController ()<tagDelegate>
//模块名称数组
@property (nonatomic, strong) NSArray *titleArray;
//图片名称数组
@property (nonatomic, strong) NSArray *picArray;
@property (nonatomic, strong) UIViewController *viewController;
@property (nonatomic, strong) TColorfulTabBar *tabBar;
@property (nonatomic, strong) UIButton *button;
@end

@implementation TabBarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.titleArray = [NSArray arrayWithObjects:@"首页",@"发现",@"",@"好友",@"我的", nil];
    // Do any additional setup after loading the view.
    UITabBarController *tabBarController = [UITabBarController new];
    for (int i = 0; i < 5; i ++) {
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 375, 667)];
        [imageView setImage:[UIImage imageNamed:@"gaoxiao.jpg"]];
        self.viewController = [UIViewController new];
        self.viewController.view.backgroundColor = [UIColor colorWithHex:0x212F3F];
        self.viewController.title = self.titleArray[i];
        self.viewController.view.tag = i;
        [self.viewController.view addSubview:imageView];
        [tabBarController addChildViewController:self.viewController];
        NSLog(@"%ld",(long)self.viewController.view.tag);
    }
    self.tabBar = [[TColorfulTabBar alloc] initWithFrame:tabBarController.tabBar.frame];
    self.tabBar.getTDelegate = self;
    [tabBarController setValue:self.tabBar forKey:@"tabBar"];
    [self.view addSubview:tabBarController.view];
    [self addChildViewController:tabBarController];
}

- (void)dS:(NSInteger)index {
        if (index == 0) {
            [self storyboardWithName:@"ShowPic" withIdentifier:@"show"];
        }else if(index == 1) {
            [self storyboardWithName:@"Find" withIdentifier:@"discover"];
        }else if (index == 2) {
            [self storyboardWithName:@"SendPicture" withIdentifier:@"send"];
        }else if (index == 3) {
            [self storyboardWithName:@"Friends" withIdentifier:@"friend"];
        }else {
            [self storyboardWithName:@"Personal" withIdentifier:@"userDefault"];
        }
}

- (void)storyboardWithName:(NSString *)name withIdentifier:(NSString *)identifier {
    UIViewController *viewController = [[UIStoryboard storyboardWithName:name bundle:nil] instantiateViewControllerWithIdentifier:identifier];
    [self presentModalViewController:viewController animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
