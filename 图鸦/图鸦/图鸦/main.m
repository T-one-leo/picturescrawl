//
//  main.m
//  图鸦
//
//  Created by brother on 15/10/16.
//  Copyright (c) 2015年 com.qizhi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
